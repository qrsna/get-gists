# A web application to get a users gists from Github

The app is built with NodeJS and EJS

The app runs by default on `http://localhost:3000`

The docker app runs on port `8080` as specified with the env var `PORT` within the Dockerfile.

# Running app on localhost
```bash
npm start
```
Access the app in your browser on: `http://localhost:3000`

# Build Dockerfile
```bash
docker build -t infinitetutts/get-gists:latest .
```

# Running app in Docker
```bash
docker run --rm -d --name get-gists -p 8080:8080 infinitetutts/get-gists:latest
```
Access the app in your browser on: `http://localhost:8080`


# Running app in Kubernetes

## Create pods and loadbalancer
Create pods
```bash
kubectl create -f deploy_kubernetes/get-gists-controller.yml
```

Create load balancer service which exposes a random port
```bash
kubectl create -f deploy_kubernetes/get-gists-rc-service.yml
```

## Test
Make sure the pods and services have been created
```bash
 ▲ projects/gitlab/get-gists kubectl get pods,svc            
NAME                  READY     STATUS    RESTARTS   AGE
pod/get-gists-cnzml   1/1       Running   0          6m
pod/get-gists-kh48n   1/1       Running   0          6m
pod/get-gists-x8nw2   1/1       Running   0          6m

NAME                 TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
service/get-gists    LoadBalancer   10.105.82.161   <pending>     8080:32595/TCP   6m
service/kubernetes   ClusterIP      10.96.0.1       <none>        443/TCP          2d
```

The exposed port in my case is `32595`.
My minikube node IP is: `192.168.99.100`

In my case I can access the app in my browser on: `http://192.168.99.100:32595`

Fire 100 asynchronous requests
```bash
for i in {1..100}; do curl http://192.168.99.100:32595 & ;done
```


## Cleaning up
Delete pods
```bash
kubectl delete -f deploy_kubernetes/get-gists-controller.yml
```

Delete load balancer
```bash
kubectl delete -f deploy_kubernetes/get-gists-rc-service.yml
```
