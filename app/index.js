const rp = require('request-promise');

var exports = module.exports = {};

exports.getGists = function(user) {
    const options = {
        uri: `https://api.github.com/users/${user}/gists`,
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true // Automatically parses the JSON string in the response
    };

    // Get request to github api
    return rp(options)
    .then(function (body) {
        console.log(`${user} has ${body.length} gists`);
        return body;
    })
    .catch(function (err) {
        // Api call failed
        console.log(err);
        return err;
    });
} 
