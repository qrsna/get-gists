const express = require("express");
const app     = express();
const path    = require("path");
const parser = require('body-parser');
const gitGists = require('./app/index.js');

// parse all data in json
app.use(parser.urlencoded({ extended: false }))
app.use(parser.json())
 
// set user value to null
app.use(function(req,res,next){
    res.locals.userValue = null;
    next();
})
 
// set the view engine to ejs
app.set('view engine','ejs');
app.set('views',path.join(__dirname,'views'))
 

// index page 
app.get('/',function(req,res){

    const gists = [];
    res.render('index',{
        gists: gists,
    });
    console.log('user accessing Home page');
});

// show gists on index page
app.post('/gists',async function(req,res){

    const user = req.body.user;
    const gists = await gitGists.getGists(user); 

    res.render('index',{
        gists: gists,
        userValue : user
    });
     
});

// run server
const port = process.env.PORT || 3000;
app.listen(port, function () {
  console.log(`App listening on port ${port}`);
});
