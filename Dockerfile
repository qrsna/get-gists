FROM node:10.6.0-alpine 
MAINTAINER Qrsna
ENV PORT=8080

WORKDIR /opt/app
COPY . ./
RUN npm i

EXPOSE 8080
CMD npm start
